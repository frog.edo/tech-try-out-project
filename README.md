Make New Repo:
Name : Assessment
With 2 file
Answer.js -> Your code
Readme.md -> Cases
Cases : 
- create a function that accept two string parameter
- your function should be able to determine the first parameter is anagram with from the second parameter vice versa
- anagram will happen if the amount of each letter from first parameter is exactly equal with the second parameter and vice versa
- no googling / open repo / project
- no build in function (split, filter, sort, map, etc)
- examples:
  - `anagram('aaz', 'zza')` => false
  - `anagram('anagram', 'nagaram'))` => true
