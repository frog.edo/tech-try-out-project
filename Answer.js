function anagram(str1, str2) {
  str1 = str1.split("");
  str2 = str2.split("");
  let hitung = {};
  for (let i = 0; i < str1.length; i++) {
    const element = str1[i];
    if (hitung[element] === undefined) {
      hitung[element] = 1;
    } else {
      hitung[element] = hitung[element] + 1;
    }
  }
  let hitung2 = {};
  for (let i = 0; i < str2.length; i++) {
    const element = str2[i];
    if (hitung2[element] === undefined) {
      hitung2[element] = 1;
    } else {
      hitung2[element] = hitung2[element] + 1;
    }
  }
  console.log(hitung);
  console.log(hitung2);
}
console.log(anagram("aaz", "zza"));
console.log(anagram("anagram", "nagaram"));
