function spinWords(string) {
  let splitedString = string.split(" ");
  let reverseWord = splitedString.map((word) => {
    if (word.length >= 5) {
      return word.split("").reverse().join("");
    }
    return word;
  });
  return reverseWord.join(" ");
}
console.log(spinWords("Hey fellow warriors"));
console.log(spinWords("This is a test"));
console.log(spinWords("This is another test"));
