Use old repo
Readme2.md -> case
Answer2.js -> Your Code
Case :
- create a function that accept single parameter. the parameter will be an array of integer
- your function should be able to count the unique value inside the array
- examples:
  - `countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]))` => 7
  - `countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]))` => 4
  - `countUniqueValues([]))` => 0